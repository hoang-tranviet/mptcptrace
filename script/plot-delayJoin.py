#! /usr/bin/env python

# INPUT: delay-join.txt

# OUTPUT:
#	plot delay-join
#	query long delay

import os
import argparse
from subprocess import call
 
delay = []

def getData(f):
	data = []
	for l in f:
		if l.startswith('#'):
			continue
		conn = l.strip().split('\t')

		if len(conn) > 1:
			data.append(float(conn[1]))

			if float(conn[1]) > 10000:
				print conn
	return data

def main(dir, outdir ):
	print os.getcwd()
	f2 = open(dir +'delay-join.txt','r+')

	delay = getData(f2)


#plot
	import numpy as np
	import matplotlib.pyplot as plt

	print len(delay)

	fig, ax = plt.subplots()

	x=np.sort(delay)
	cdf=np.arange(len(x))/float(len(x))
	ax.plot( x, cdf, color = 'red')
	
	ax.set_xscale('log')
	plt.xlabel('Time of JOINs occurence from CAPA_SYN (s)')
	plt.ylabel('CDF')
	plt.savefig(outdir +'delay-join.pdf', bbox_inches='tight')
	plt.show()

if __name__ == '__main__':
	dir = './'
	main(dir, dir)
