#! /usr/bin/env python

# INPUT like this:

# OUTPUT: 
#   find out path manager (only consider mpconn with >= 2 sf) -> cdf

import os
from IPy import IP
STATE = 0
BYTE_C2S = 1
BYTE_S2C = 2
IP_SRC = 3
IP_DST = 4

count = {}

def find_pm(sfs):
	isf = sfs[len(sfs)-1]	# get the last element

	pm = 'NPORTS'
	for sf in sfs:
		if IP(sf[IP_SRC]) != IP(isf[IP_SRC]) or IP(sf[IP_DST]) != IP(isf[IP_DST]):
			pm = 'MESH'
	return pm

def process(f, of):
	s = f.split('_')
	conn_id = s[2]

	sfs = []
	for l in open(f):
		sf = l.strip().split(',')
		sfs.append(sf)

	if len(sfs) < 2:
		return
	
	pm = find_pm(sfs)
	of.write(str(conn_id) + '\t' +str(len(sfs))+ '\t'+ pm +'\n')

	count[pm] += 1
	return pm

def main(dir, outdir):
	count['MESH']= 0
	count['NPORTS']= 0

	of = open(outdir +'/'+ 'pm.txt','w+')

	of.write("# ConnID	sf_count	path_Manager\n")
	for f in sorted(os.listdir(dir)):
		if f.startswith('sf_conn_'):
			process(f, of)
	print count
