#!/bin/bash

cd ../../imc/mptcponly/
rsync -e "ssh -i ~/.ssh/id_rsa"  --ignore-existing  -vhP hoang@nos:/home/mpcollect/collect/mptcp167/*.gz  .

# todo: if trace not exist -> unzip if did not -> copy trace
for zipfile in *.gz 
do
	trace=${zipfile%.tar.gz}
	
	file=${trace}.pcap
	# copy trace from subfolder if it doesn't exist.
	if [ ! -f $file ]; then
		# if folder not exist, unzip
		if [ ! -d "$trace" ]; then 
			echo "extracting trace: ${trace}"
			tar --keep-old-files -xf  ${zipfile}
		fi
		cd ${trace}
	#	editcap  -F libpcap  *.pcap  ../${trace}.pcap
		mv *.pcap  ../${trace}.pcap
		cd ..
		# remove folder
		rm -r $trace
	fi
done


