#!/bin/bash

in=$1
conn=$2
out=$3
tcpdump -r $in -w ${out} $(cat stats_${conn}.csv | grep Filter | cut -d ";" -f 4)