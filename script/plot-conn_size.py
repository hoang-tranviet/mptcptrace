#! /usr/bin/env python

# INPUT: mp-data.txt, sf-data.txt

# OUTPUT:
#	plot conn duration

import os
import argparse
from subprocess import call
 
mpBytes = []
sfBytes = []

def getData(f):
	data = []
	for l in f:
		if l.startswith('#'):
			continue
		conn = l.split('\t')
		if len(conn) > 1:
			data.append(long(conn[1]))
	return data

def main(dir, outdir ):
	print os.getcwd()
	print 'Total bytes of each connection'
	f2 = open(outdir +'mp-data.txt','r+')
	f3 = open(outdir +'sf-data.txt','r+')

	mpBytes = getData(f2)
	sfBytes = getData(f3)


#plot
	import numpy as np
	import matplotlib.pyplot as plt

	print len(sfBytes),len(mpBytes)

	fig, ax = plt.subplots()
	x=np.sort(sfBytes)
	cdf=np.arange(len(x))/float(len(x))
	ax.plot( x, cdf, ls = '--', label='subflows')

	x=np.sort(mpBytes)
	cdf=np.arange(len(x))/float(len(x))
	ax.plot( x, cdf, color = 'red', label='MPTCP connections')
	
	ax.set_xscale('log')
	ax.legend( loc = 'lower right')
	plt.xlabel('Connection size (Bytes)')
	plt.ylabel('CDF')
	plt.show(block=False)
	plt.savefig(outdir +'mpBytes.eps', bbox_inches='tight')

if __name__ == '__main__':
	dir = '.'
	main(dir, dir + '/out/')
