#! /usr/bin/env python

# INPUT like this:
# $ cat sf_conn_49
# 2,0,0
# 2,1,1				// this sf should contain datafin
# 2,0,0
# 2,0,0
# 3,2026,3640			// last line is isf!

# OUTPUT: (only consider mpconn having 2 sf)
#	fraction of data via isf over total,  -> cdf
#	bytes/packets over mpconn/isf  -> cdf ;	use tcptrace?

import os
from time import sleep
from subprocess import call
from IPy import IP
import pathmanager
STATE = 0
BYTE_C2S = 1
BYTE_S2C = 2
IP_SRC = 3
IP_DST = 4
PORT_S = 5
PORT_D = 6

SYN 	= 1
SYNACK 	= 2
ACK 	= 3

ISFRatio = []
ISFRatio_10MB = []

def process(f, of):
	s = f.split('_')
	conn_id = s[2]

	sfs = []

	for l in open(f):
		sf = l.strip().split(',')
		if l[0] == '#':
			continue
		sfs.append(sf)

	isf = sfs[-1]	# get the last element

	# remove sf not finished 3wHS, except isf
	for sf in sfs:		
		if int(sf[STATE]) < ACK and sf != isf:
			sfs.remove(sf)

	if len(sfs) != 2:
		return

	sf1= sfs[0]
	sf2= sfs[1]
	sf1bytes = long(sf1[BYTE_C2S]) + long(sf1[BYTE_S2C])
	sf2bytes = long(sf2[BYTE_C2S]) + long(sf2[BYTE_S2C])
	totalByte = sf1bytes + sf2bytes

	if totalByte < 100000:	# > 100 KB
		return

	isfRatio = float(sf2bytes) / totalByte
	of.write(conn_id +'\t'+ str(isfRatio)+'\n')

	ISFRatio.append(isfRatio)
	if totalByte < 10000000:
		ISFRatio_10MB.append(isfRatio)

def getData(dir, outdir):
	print os.getcwd()

	of = open(outdir +'isf-data-2sf.txt','w+')
	of.write("# ConnID		%isf_bytes\n")
	for f in sorted(os.listdir(dir)):
		if f.startswith('sf_conn_'):
			# sleep(0.1)
			process(dir+f, of)

#plot
	import numpy as np
	import matplotlib.pyplot as plt

	print len(ISFRatio)

	fig, ax = plt.subplots()

	x=np.sort(ISFRatio)
	cdf=np.arange(len(x))/float(len(x))
	ax.plot( x, cdf, color = 'red')
	
	# ax.set_xscale('log')
	plt.xlabel('Percentage of data sent via ISF over all data')
	plt.ylabel('CDF')
	plt.savefig(outdir +'isf-data-2sf.pdf', bbox_inches='tight')
	plt.show()

	fig, ax = plt.subplots()

	x=np.sort(ISFRatio)
	cdf=np.arange(len(x))/float(len(x))
	ax.plot( x, cdf, 	color = 'red', label='> 100 KB')
	x=np.sort(ISFRatio_10MB)
	cdf=np.arange(len(x))/float(len(x))
	ax.plot( x, cdf,'--' ,color = 'blue', label='> 10 MB')
	
	ax.legend(loc='best')
	# ax.set_xscale('log')
	plt.xlabel('Percentage of data sent via ISF over all data')
	plt.ylabel('CDF')

	plt.savefig(outdir +'isf-data-2sf-multilines.pdf', bbox_inches='tight')
	plt.show()


if __name__ == '__main__':
	dir = '../'
	getData(dir,'./')
