#! /usr/bin/env python

# INPUT: stats_*.csv

# OUTPUT:
#	conn duration

import os
import argparse
from subprocess import call
TRACE = 0
CONN  = 1
TYPE  = 2
VAL   = 3

durations = []


def process(f, of):
	s = f.split('_')
	conn_id = s[1]

	for line in open(f):
		l = line.strip().split(';')

		if l[TYPE] == 'conTime':
			duration = float(l[VAL])
			durations.append( duration )
			of.write( l[VAL] + '\n')
			if duration > 10000:
				print f, duration
			return

def main(dir, plot=False):
	of = open('conTime.txt', 'w')

	for f in os.listdir(dir):
		if f.startswith('stats_'):
			process(f, of)

	print  len(durations)
	if plot:
		doplot()

def doplot():
	import numpy as np
	import matplotlib.pyplot as plt

	fig, ax = plt.subplots()

	x=np.sort(durations)

	cdf=np.arange(len(x))/float(len(x))
	ax.plot( x, cdf, label='connection duration')

	ax.set_xscale('log')

	ax.legend()
	plt.show()
	plt.savefig('out/conTime.pdf', bbox_inches='tight')

if __name__ == '__main__':
	parser = argparse.ArgumentParser( description = "get connection duration")
	parser.add_argument('--plot', '-p', default= False, action='store_true')
	args = parser.parse_args()

	dir = './'
	main(dir,args.plot)
