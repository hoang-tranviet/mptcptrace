#!/bin/bash

cd ../../imc/mptcponly/

# to merge tiny traces

#clean
rm *.merge*

lastTrace='mptcp167.1416216927.V6v.pcap'

i=0
tracelist=" "
for trace in *.pcap
do
	((i+=1))
	tracelist+=$trace
	tracelist+=" "

	# every 100 tiny traces will be merged into one
	if [[ ( "$i" -eq 100  ) || ( "$trace" == "$lastTrace" ) ]]
	then
		cmd="mergecap  -a "
		out=${trace%.pcap}.merge.pcap
		cmd+=" -w  $out "
		cmd+=$tracelist

		echo $out
		eval $cmd

		# reset list for the next merge
		tracelist=" "
		((i=0))
	fi
done