#! /usr/bin/env python

import os
from subprocess import call
import numpy as np
import matplotlib.pyplot as plt

import argparse

parser = argparse.ArgumentParser(description="Wrapper for mptcptrace")
parser.add_argument('--dir', '-d',
                    help="Directory to store outputs",
					default="./")
args = parser.parse_args()

def neighborhood(iterable):
    iterator = iter(iterable)
    prev = None
    item = iterator.next()  # throws StopIteration if empty.
    for next in iterator:
        yield (prev,item,next)
        prev = item
        item = next
    yield (prev,item,None)

import math
from decimal import *
from scipy.stats import binned_statistic
# real time data sent over each subflow
dataSent = {}

def getDataDistribution(dir, timeslot):
							# in ms
	print(os.getcwd() + "\n")
	f = open(str(dir) + '/s2c_seq_1.xpl' ,'r')

	lines = f.readlines()

	for prev,line,next in neighborhood(lines):
		if line.startswith('line'):
			l = line.strip().split(' ')
			# this causes rounding error (sai so lam tron)
			# better to use Decimal type, but np.digitize/scipystats only support float type
			time = float(l[1])
			bytes = long(l[4]) - long(l[2])	# Ack# - Seq#
			sf = int(prev)
			if sf not in dataSent:
				dataSent[sf] = []
			dataSent[sf].append([time, bytes])
	print len(dataSent[1])

	mp_start = mp_end = dataSent[1][0][0]

	for sf in dataSent:
		data = np.array(dataSent[sf])
		time = data[:,0] # get the first column
		data[time.argsort()] # sort by time

		x_time = data[:,0]
		bytes = data[:,1]
		start = min(x_time)
		end   = max(x_time)
		mp_start = min(start, mp_start)
		mp_end   = max(end, mp_end)

#		what i want is to binning on time (keys) and then compute on bytes(values)
#		cannot use  np.digitize
#		digitized = np.digitize(x_time, bins)
	# plot individually
		# plt.figure()
		# plotByte(x_time, bytes, start, end, timeslot)


	for sf in dataSent:
		data = np.array(dataSent[sf])
		x_time = data[:,0]
		bytes  = data[:,1]
		print mp_start, mp_end, timeslot
		plt.figure(1)	# specify the common figure
		plotByte(x_time, bytes, mp_start, mp_end, timeslot)

	plt.show(block=False)
	plt.savefig('Bytes')

def plotByte(x_time, bytes, start, end, timeslot):
	bins = np.arange(start, end, step = (timeslot))

	print len(x_time), len(bins), start, end

	bin_sums = binned_statistic(x_time, bytes, statistic='sum', bins=len(bins), range=[(start, end)] ) [0]
	print bin_sums
	plt.plot(bins, bin_sums)



def getGraphs(dir, n):
	outdir = 'pcapExtract/'+str(n)
	os.chdir(outdir)
	tcptrace = 'tcptrace -R -T -S -n '+ str(n)+'.pcap'
	print tcptrace
	call(tcptrace, shell=True)
	call("rm  a2b_* c2d_* ", shell=True)

	mp = 'mptcptrace -s -S -f '+ str(n)+'.pcap'
	print mp
	call(mp, shell=True)
	call("rm  c2s* mptcptrace* ", shell=True)

#	getDataDistribution(".", 0.2)
	os.chdir(dir)

def main(dir, outdir):
	getDataDistribution(dir, 0.2)
	raw_input('Showing figures')

if __name__ == '__main__':
	dir = args.dir
	main(dir,os.getcwd())

