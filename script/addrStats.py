#! /usr/bin/env python

# INPUT like this:
#
# hoang$ cat conn_40 
# 931,1407352272.625263,isf,2001:200:164:33:10:11:10:2,2001:6a8:3080:1:216:3eff:fec5:c815,v6
# 934,1407352272.920091,add_addr,1,10.11.10.2,1
# 935,1407352272.920114,add_addr,1,130.104.230.45,0

# OUTPUT: privateIP/totalIP address count.
#	num of addaddress per connection  
# I added link-local to IP.IPy, otherwise 'PRIVATE' also contains IPv4 link-local addr: 169.254..

from IPy import IP
import os

TYPE = 2
ID   = 3
ADDR = 4
WAY  = 5
count = {}	# counter for each type of IP addAddress
countV4 = {}	# counter for each type of IPv4 addAddress
countV6 = {}	# counter for each type of IPv6 addAddress
n_addaddr=[]  #num of addaddress per connection

def process(f):

	n_addaddr.append(0)

	for l in open(f):
		if l[0]=='#':
			continue			
		col = l.strip().split(',')

		if (col[TYPE] != 'add_addr') or col[WAY] != '1':
			continue
		ip = IP(col[ADDR])
		# print ip, ip.iptype()
		type = ip.iptype()
		if type not in count:
			count[type] = 1
		else:
			count[type] += 1

		if ip.version() == 4:
			if type not in countV4:
				countV4[type] = 1
			else:
				countV4[type] += 1
		else:
			if type not in countV6:
				countV6[type] = 1
			else:
				countV6[type] += 1

	#	if ip.iptype() == 'RESERVED': 
	#		print ip, ip.iptype()
			
		n_addaddr[-1] += 1	# the last element

def getStats(dir, outdir):
	total =0
	conn_total=0

	os.chdir(dir)
	for f in os.listdir(dir):
		if f.startswith('conn_'):
			#print f,
			conn_total += 1
			process(f)

	conn_w_add = 0
	for n in n_addaddr:
		if n > 0:
			conn_w_add+=1

	print 'num of conns having addaddr:', conn_w_add
	print count
	print countV4
	print countV6

	print 'total IP4:', sum(countV4[i] for i in countV4)
	print 'total IP6:', sum(countV6[i] for i in countV6)

	total= sum(count[i] for i in count)

#	print "connections in total (maybe 3HS not completed) \t",conn_total

	nonpublic = count['PRIVATE'] + count['ULA'] + count['LINKLOCAL'] + count['DOCUMENTATION'] + count['RESERVED'] + count['6TO4']
	print 'total\t', total
	print 'non-pub\t', nonpublic
	print 'public\t', (total - nonpublic)

	of = open(outdir +'/addAddr.txt','w+')
	for t in count:
		of.write( t+'\t'+ str(count[t]) +'\n')

if __name__ == "__main__":
# for direct script execution from command line:  $ ./addrStats.py
	dir = '../'
	getStats(dir)
