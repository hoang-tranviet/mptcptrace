#! /usr/bin/env python

# INPUT like this:
# mptcptrace/src$ cat sf_conn_49
# 2,0,0
# 2,1,1				// this sf should contain datafin
# 2,0,0
# 2,0,0
# 3,2026,3640			// last line is isf!

# OUTPUT:
#	num of sf each conn

import os
from subprocess import call
STATE = 0
sfcount = []

def process(f):
	s = f.split('_')
	conn_id = s[2]
	sfs = []

	for l in open(f):
		sf = l.strip().split(',')
		if int(sf[STATE]) < 3:		# SYN =1, SYNACK = 2,  ACK = 3
			continue		# only consider conn with isf finish 3wHS
		sfs.append(sf)
	if len(sfs) == 0:
		return
	sfcount.append(len(sfs))

def main(indir, outdir):
	# print os.getcwd()
	SFCountMax = 128
	realMax = 0
	sfCountHis = [0]*SFCountMax

	of = open(outdir +'/'+'sf-count.txt','w+')
	for f in sorted(os.listdir(indir)):
		if f.startswith('sf_conn_'):
			process(indir +'/'+ f)
	for n in sfcount:
		sfCountHis[n] += 1
		if realMax < n:
			realMax = n
	print 'total number of successful subflows:', sum(sfcount) 
	print 'total number of successful connections:', sum(sfCountHis)
	for n in range(realMax+1):
		of.write(str(n) +'\t'+ str(sfCountHis[n])+'\n')
#plot
	import numpy as np
	import matplotlib.pyplot as plt

	fig, ax = plt.subplots()
	x = range(SFCountMax)
	ba = plt.bar(x, sfCountHis, label='number of subflow (successful)', color='r')

	ax.legend()
#	plt.show(block=False)
	plt.savefig('nflows.eps', bbox_inches='tight')
# clear figure
#	plt.clf()

if __name__ == '__main__':
	indir = '../'
	main(indir, os.getcwd())
