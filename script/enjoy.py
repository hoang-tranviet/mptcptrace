#! /usr/bin/env python

# INPUT: week/day
# OUTPUT: results from all traces in the week/day
# use mptcptrace as backend

from subprocess import call
from datetime import *
from isoweek import Week
import time
import os
import argparse

parser = argparse.ArgumentParser(description="Wrapper for mptcptrace")
parser.add_argument('--dir', '-d',
                    help="Directory to store outputs",
					default="../../imc/mptcp167")
					# captured from multipath-tcp.org

parser.add_argument('--all', '-a', default= False, action='store_true',
					dest="all")		# analyse ALL traces, warning: maybe too much

parser.add_argument('--week', '-w',
					default='47',	# TMA data is got in week 47
					dest="week")

parser.add_argument('--plot', '-p',
					default=False, action='store_true',
					dest="plot")

# '-n': do not run mptcptrace (if it was used before) 
parser.add_argument('--notraceparsing', '-n', default= True,
					action='store_false', dest='parsing')
# extract desired connection to a pcap file?
parser.add_argument('--extract', '-e', default= False, action='store_true')


# Expt parameters
args = parser.parse_args()

indir = os.path.abspath(args.dir) 

if args.all:
# process all traces
	outdir = indir +'/all/'
	sday = Week(2014, 1).monday()
	eday = Week(2015, 50).sunday()
else:
	# process a certain week
	year = 2014
	week = int(args.week)
	outdir = indir + '/week/'+ str(week) +'/'

	sday = Week(year, week).monday()
	eday = Week(year, week).sunday()

if not os.path.exists(outdir):
	os.makedirs(outdir)
os.chdir(outdir)
print os.getcwd()


print 'We gonna to analyse traces from ', str(sday), ' to ', str(eday)

def date2epoch(date):
	date_time = str(date) + ' 00:00:00'
	pattern = '%Y-%m-%d %H:%M:%S'
	epoch = int(time.mktime(time.strptime(date_time, pattern)))
	return str(epoch)

def getDirName(indir):
	(path, dot, name) = indir.rpartition('/')
	return name

prefix= getDirName(indir)
start = prefix + "."+ date2epoch(sday)
end   = prefix + "."+ date2epoch(eday)

import globals
tracelist = []

# create_symlink(dir):
for trace in sorted(os.listdir(indir)):
	if trace.endswith('.pcap') and str(trace) > start and str(trace)< end:
		tracelist.append(str(trace))
		if os.path.islink(outdir+trace):	# skip if symlink is created already
			continue
		call('ln -s '+ indir+'/'+trace + '  '+ outdir+trace, shell=True)

globals.tracelist = tracelist

duration = " -b " + start + " -e " + end

if args.parsing:
	loglevel = ' -l 1 '
	cmd = "mptcptrace -d "+ outdir + " -t 10000 -v -S " + loglevel # + duration
	print cmd
	print ""
	# clean temp files:
	call("rm sf_conn_* conn_* stats*.csv", shell=True)
	return_code = call (cmd, shell=True)


# get tcptrace csv long format file. Run tcptrace if this file does not exist.
tcpFile = "rtt-tcp.csv"
if not os.path.isfile(tcpFile) or os.path.getsize(tcpFile)<1000:
	# print 'Extract RTT with tcptrace'
	call('tcptrace --csv -l -r -nt *.pcap > rtt-tcp.csv 2> /dev/null', shell=True)


import addrStats
#addrStats.getStats(dir, outdir)
import client_ip
#client_ip.getPrefixStats(dir, outdir)
import conTime
# conTime.main(outdir, False)

import nflows
# nflows.main(outdir,outdir)
import pathmanager
# pathmanager.main(outdir, outdir)

import rtt_diff
rtt_diff.main(outdir)

import data_rtt_2sf
# data_rtt_2sf.main(outdir, outdir, args.extract, args.plot)

import data_2sf
#data_2sf.getData(outdir, outdir)

if args.plot:
	raw_input('Press any key to finish')
