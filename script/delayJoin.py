#! /usr/bin/env python

# INPUT like this:
# MacBook-Pro-2009:delay hoang$ cat conn_40 
#
# 931,1407352272.625263,isf,2001:200:164:33:10:11:10:2,2001:6a8:3080:1:216:3eff:fec5:c815,v6
# 934,1407352272.920091,add_addr,1,10.11.10.2,1
# 935,1407352272.920114,add_addr,1,130.104.230.45,0
# 937,1407352273.214869,add_addr,2,203.178.154.50,1
# 938,1407352273.215025,join_syn,2,203.178.154.50,130.104.230.45,36040,80
# 940,1407352273.215390,join_syn,10,2001:200:164:48::53:1,2001:6a8:3080:1:216:3eff:fec5:c815,53511,80
# 949,1407352274.746723,add_addr,10,2001:200:164:48::53:1,1

# OUTPUT: 1) delay from isf_syn to first joinsyn.
#		  2) delay between add_addr and joinsyn
import os
import argparse


IDX = 0
TS  = 1
TYPE= 2
ID  = 3
WAY = 5


dlmax = 0
connmax = ''

def process(f, of1):
	# joined = False	# to know if there is a joinsyn
	# ts_add = {}
	ts_joinsyn = {}

	global dlmax, connmax
	for l in open(args.dir + f):

		if l[0] == '#':
			continue
		col = l.strip().split(',')
		if len(col) <= TYPE:
			continue

		# below: compute delay from isf to first join.
		if col[TYPE]=='isf':	# from client
			ts_capasyn = float(col[TS])
			continue
		if col[TYPE]=='join_syn':					
			ts_join = float(col[TS])
			# #print ts_1st_join
			# joined  = True

			dl = (ts_join - ts_capasyn)

			if dl > 1000:
				print dl, f

			if dlmax < dl:
				dlmax = dl
				connmax = f

			of1.write(str(f)+'\t' +str(dl) + '\n')


def main(dir, plot=False):

	print 'get JOIN delay'
	of1 = open('out/delay-join.txt','w+')

	for f in os.listdir(dir):
		if f.startswith('conn_'):
			#print f,
			process(f, of1)

	print 'output to delay-join.txt'

if __name__ == '__main__':
	parser = argparse.ArgumentParser( description = "get time of JOIN SYN from connection begin")

	parser.add_argument('--dir', '-d',	default="./")
	parser.add_argument('--plot', '-e', default= False, action='store_true')
	args = parser.parse_args()

	main(args.dir)