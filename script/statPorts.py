#! /usr/bin/env python

# INPUT like this:
# mptcptrace/src$ cat sf_conn_49
# 2,0,0
# 2,1,1				// this sf should contain datafin
# 2,0,0
# 2,0,0
# 3,2026,3640			// last line is isf!

# OUTPUT:
# bytes over each port 
# conns over each port

import os
from subprocess import call
STATE = 0
BYTE_C2S = 1
BYTE_S2C = 2
PORT_S = 5
PORT_D = 6

connsPerPort = {}
bytesPerPort = {}


def process(f, of2, of3):
	s = f.split('_')
	conn_id = s[2]
	sfs = []

	for l in open(f):
		sf = l.strip().split(',')
		# if int(sf[STATE]) < 3:		# SYN =1, SYNACK = 2,  ACK = 3
		# 	continue		# only consider conn with isf finish 3wHS
		sfs.append(sf)

	# if len(sfs) == 0:
	# 	return
	connByte = 0

	for sf in sfs:
		sfByte = long(sf[BYTE_C2S]) + long(sf[BYTE_S2C])

		# of3.write(conn_id+'\t'+ str(sfByte) +'\n')
		connByte += sfByte

	sport = int(sf[PORT_S])
	if sport not in connsPerPort:
		connsPerPort[sport] = 1
		bytesPerPort[sport] = connByte
	else:
		connsPerPort[sport] += 1
		bytesPerPort[sport] += connByte

	dport = int(sf[PORT_D])
	if dport not in connsPerPort:
		connsPerPort[dport] = 1
		bytesPerPort[dport] = connByte
	else:
		connsPerPort[dport] += 1
		bytesPerPort[dport] += connByte


	# of2.write(str(conn_id) + '\t' +str(connByte)+ '\n')

def main(dir, outdir ):
	print os.getcwd()
	of2 = open(outdir +'port-conn.txt','w+')
	of3 = open(outdir +'port-data.txt','w+')
	of2.write("# Port		connection_count\n")
	of3.write("# Port		byte_count\n")
	for f in os.listdir(dir):
		if f.startswith('sf_conn_'):
			process(f, of2, of3)

	totalConns = sum(connsPerPort.values())/2
	totalBytes = sum(bytesPerPort.values())/2
	print 'Total Conns:',totalConns
	print 'Total Gbytes:',totalBytes>>30

	print 'stats per port'
	print 'port  connections 	%conns 		MBs		%MB'
	for port in connsPerPort:
		if connsPerPort[port] > 50:
			print port, '\t', connsPerPort[port],  '\t\t', round(float(connsPerPort[port])/totalConns, 5)*100, 
			print '\t\t', (bytesPerPort[port]>>20 ), '\t', round(float(bytesPerPort[port])/totalBytes, 5)*100	# convert to MB

#plot
	# import numpy as np
	# import matplotlib.pyplot as plt

	# print len(sfData),len(mpData)

	# fig, ax = plt.subplots()
	# x=np.sort(sfData)
	# cdf=np.arange(len(x))/float(len(x))
	# ax.plot( x, cdf, label='subflow bytes')

	# x=np.sort(mpData)
	# cdf=np.arange(len(x))/float(len(x))
	# ax.plot( x, cdf, label='mptcp bytes')
	
	# ax.set_xscale('log')
	# ax.legend( loc = 'lower right')
	# plt.show()
	# plt.savefig(outdir +'stat-ports.eps', bbox_inches='tight')

if __name__ == '__main__':
	dir = '.'
	main(dir, dir + '/out/')
