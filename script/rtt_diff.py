#! /usr/bin/env python

#INPUT: 1)tcptrace long format csv, with RTT option: 
	#	2)mptcptrace: subflow's tuple of each conn

#OUTPUT: Link the RTT value to each subflow.
	#	Max(sfs) - min(sfs)

import os
import re
from IPy import IP
import argparse

# column in sf_conn_{id}
STATE = 0
BYTE_C2S = 1
BYTE_S2C = 2
IPSRC = 3
IPDST = 4
SPORT =	5
DPORT =	6

ackPktC2S = 11

colID ={}
rtt ={}
RTT_avg=[]
rttFile = "rtt-tcp.csv"


def loadRTT():
	for l in open(rttFile):
		if l[0]=='#':
			continue

		col = l.strip().split(',')
		if len(col) < 40:
			continue

		if col[0] == 'conn_#':
			field = col

			for id in range(len(field)):
				# print id, field[id]
				colID [field[id]] = id
			continue


		# #  this is IPv4-comp (::91.109.28.55), tcptrace incorrectly print, 
		if re.match( '^:', col[1]):
			sIP = IP(":" + col[1])
		else: 
			sIP = IP(col[1])	# str to IP, using IPy.IP package


		dIP = IP(col[2])
		sport = col[3]
		dport = col[4]

		tuple = (sIP, dIP, sport, dport)		# beware that they are string!
		# print tuple

		# if col[1] =='148.251.69.238' and sport=='39088':
		# 	print tuple, rtt_avg, col[ackPktC2S], col[7:13], col[96], col[91:95]

		# rtt_avg: colID = 95 
		rtt_avg =  float( col[colID['RTT_avg_b2a'] ]) 


		if rtt_avg > 0:
			rtt[tuple] = rtt_avg 



def getRTT(sf):
	tuple = (IP(sf[IPSRC]), IP(sf[IPDST]), sf[SPORT], sf[DPORT])
	if tuple in rtt:
		return rtt[tuple]
	else:
		# print  sf, 'not in tcptrace output, or RTT = 0'
		return 0

def process(f, of):
	s = f.split('_')
	conn_id = s[2]

	sfs = []

	for l in open(f):
		sf = l.strip().split(',')
		if int(sf[STATE]) < 3:		# SYN =1, SYNACK = 2,  ACK = 3
			continue
		sfs.append(sf)

	if len(sfs) < 2:	# only consider conn having at least 2 sf.
		return

	byteS2C = 0
	for sf in sfs:
		byteS2C += long(sf[BYTE_S2C])

	rtts=[]
	for sf in sfs:
		rtt = getRTT(sf)
		if rtt == 0:
			# print conn_id, sf, 'not in tcptrace output'
			return
		rtts.append(rtt)


	# print 'max=',maxRTT, 'min=', minRTT, maxRTT - minRTT
	diffRTT = max(rtts) - min(rtts)
	if diffRTT ==0:
		diffRTT = 0.1

	of.write(str(conn_id) + '\t' + str(byteS2C)+ '\t' + str(min(rtts)) + '\t' + str(max(rtts)) + '\t' + str(diffRTT) + '\n')

def main(dir):
	of = open("rtt-diff.txt", "w+")
	of.write("#conn_id BytesSent	minRTT(sfs)	maxRTT(sfs)	(MaxRTT(sfs) - minRTT(sfs)) \n")

	loadRTT()

	for f in sorted(os.listdir(dir)):
		if f.startswith('sf_conn_'):
			# print f
			process(dir+f, of)


if __name__ == '__main__':
	parser = argparse.ArgumentParser( description = "get RTTdiff = MaxRTT(sfs) - minRTT(sfs)")
	parser.add_argument('--dir', '-d', default= './')

	args = parser.parse_args()

	main(args.dir)