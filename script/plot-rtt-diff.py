#! /usr/bin/env python

# INPUT: out/rtt-diff.txt (this is inturn output of rtt-diff.py)

# OUTPUT:
#	plot diff between fastest SF and slowest SF of a MPTCP conn.
#	can set condition

import os
import argparse
from subprocess import call
 
BYTES  = 1
RTTDIFF= 4

rttdiff = []


def getData(f):
	data = []
	for l in f:
		if l.startswith('#'):
			continue
		conn = l.strip().split('\t')

		size = float(conn[BYTES])
		value = float(conn[RTTDIFF])

		if len(conn) > 2 and size > 100000:
			data.append(value)

			if value > 1000:
				print conn
	return data

def main(dir, outdir ):
	print os.getcwd()
	f2 = open(dir +'rtt-diff.txt','r+')

	rttdiff = getData(f2)


#plot
	import numpy as np
	import matplotlib.pyplot as plt

	print len(rttdiff)

	fig, ax = plt.subplots()

	x=np.sort(rttdiff)
	cdf=np.arange(len(x))/float(len(x))
	ax.plot( x, cdf, color = 'red')
	
	ax.set_xscale('log')
	plt.xlabel('Difference of average RTT between worst and best subflows (ms)')
	plt.ylabel('CDF')
	plt.savefig(outdir +'rtt-diff.pdf', bbox_inches='tight')
	plt.show()

if __name__ == '__main__':
	dir = './'
	main(dir, dir)
