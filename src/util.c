//#include "util.h"

#include <netinet/in.h>
#include <arpa/inet.h>

// This is adapted from libtrace demo

/* Given a sockaddr containing an IP address, prints the IP address to stdout
 * using the common string representation for that address type */


char * my_ntop( sa_family_t family, struct in_addr *ip) {
	if (family == AF_INET) {
		/* Use inet_ntop to convert the address into a string using
		 * dotted decimal notation */
		char str[INET_ADDRSTRLEN+1];
		inet_ntop(AF_INET, &ip, str, INET_ADDRSTRLEN+1);
		return str;
	}

	if (family == AF_INET6) {
		char str[INET_ADDRSTRLEN6+1];
		/* Use inet_ntop to convert the address into a string using
		 * IPv6 address notation */
		return inet_ntop(AF_INET6, &ip, str, INET_ADDRSTRLEN6+1));
		return str;
	}
	else perror();
}


inline void print_ip(FILE *f, struct sockaddr *ip) {

	char str[20];
	
	/* Check the sockaddr family so we can cast it to the appropriate
	 * address type, IPv4 or IPv6 */
	if (ip->sa_family == AF_INET) {
		/* IPv4 - cast the generic sockaddr to a sockaddr_in */
		struct sockaddr_in *v4 = (struct sockaddr_in *)ip;
		/* Use inet_ntop to convert the address into a string using
		 * dotted decimal notation */
		fprintf(f,"%s ", inet_ntop(AF_INET, &(v4->sin_addr), str, 20));
	}

	if (ip->sa_family == AF_INET6) {
		/* IPv6 - cast the generic sockaddr to a sockaddr_in6 */
		struct sockaddr_in6 *v6 = (struct sockaddr_in6 *)ip;
		/* Use inet_ntop to convert the address into a string using
		 * IPv6 address notation */
		fprintf(f,"%s ", inet_ntop(AF_INET6, &(v6->sin6_addr), str, 20));
	}
}

