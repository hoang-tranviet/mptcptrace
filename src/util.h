/* util.h
 *
 *  Created on: Oct 31, 2014
 *      Author: Tran Viet Hoang
 */

#ifndef UTIL_H_
#define UTIL_H_

#include <string.h>

char * my_ntop(sa_family_t  family, struct sockaddr *ip)  ;
inline void print_ip(FILE *f, struct sockaddr *ip);


int isWrongAddress(uint32_t * ip_p)
{
    uint32_t ip = ntohl(*ip_p);
    uint8_t b1, b2, b3, b4;
    b1 = (uint8_t)(ip >> 24);
    b2 = (uint8_t)((ip >> 16) & 0x0ff);
    b3 = (uint8_t)((ip >> 8) & 0x0ff);
    b4 = (uint8_t)(ip & 0x0ff);
    
    // loopback
    if (b1 == 127)
        return 1;
 
    // multicast
    if ((b1>=224)&&(b1<=239))
	return 2;
    // broadcast
    if (b4 == 255)
        return 3;
    // link_local
    if ((b1 == 169) && (b2 == 254))
        return 4;
    
    return 0;

}

int isPrivateAddress(uint32_t * ip_p)
{
    uint32_t ip = ntohl(*ip_p);
    uint8_t b1, b2, b3, b4;
    b1 = (uint8_t)(ip >> 24);
    b2 = (uint8_t)((ip >> 16) & 0x0ff);
    b3 = (uint8_t)((ip >> 8) & 0x0ff);
    b4 = (uint8_t)(ip & 0x0ff);
    
//	fprintf(stderr, "%d %d %d %d \n", b1, b2, b3,b4);
   
    // 10.x.x.x
    if (b1 == 10)
        return 1;
 
	// 172.16.0.0 - 172.31.255.255
    if ((b1 == 172) && (b2 >= 16) && (b2 <= 31))
        return 1;

    // 192.168.0.0 - 192.168.255.255
    if ((b1 == 192) && (b2 == 168))
        return 1;

    return 0;
}

int isWrongAddressV6(void * ip_p)
{
	struct in6_addr * ip;
	ip = (struct in6_addr *) ip_p;
	
	// most of ipv6 addresses have type = 1 (unicast) 
	if (ipv6_addr_type(ip)  != 1)
	{	fprintf(stderr," ipv6_addr_type(ip) = %X \n", ipv6_addr_type(ip));
		return 1;
	}
	return 0;
}

#endif /* UTIL_H_ */
