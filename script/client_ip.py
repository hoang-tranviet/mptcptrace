#! /usr/bin/env python

# INPUT:  sf_conn
# OUTPUT: 
# count distinctive client IPs  

import os
from IPy import IP
STATE = 0
IP_SRC = 3
IP_DST = 4

serverIP = set(["2001:6a8:3080:1:216:3eff:fec5:c815","130.104.230.45"])
clientIPs = set()

v4net=set()
v6net=set()


def process(f):
	s = f.split('_')
	conn_id = s[2]

	sfs = []	# list of subflows in a connection
	for l in open(f):
		sf = l.strip().split(',')
		sfs.append(sf)

	isf = sfs[len(sfs)-1]	# get the last element

	for sf in sfs:
		ip = (sf[IP_SRC])
		if ip not in clientIPs:
			clientIPs.add(ip)

	return 

def getPrefixStats(dir, outdir):
	ipv4 = 0
	ipv6 = 0
	of = open( outdir +'/ip-prefixes.txt','w+')
	print os.getcwd()

	for f in sorted(os.listdir(dir)):
		if f.startswith('sf_conn_'):
			process(f)

	for ip in clientIPs:
		if IP(ip).version() == 4:
			ipv4 += 1
			# trim the right most 8-bit -> prefix /24
			(prefix, dot, suffix) = ip.rpartition('.')
			v4net.add(prefix)
		elif IP(ip).version() ==6:
			ipv6 += 1
			#ip6 = IP(ip).make_net('ffff:ffff:ffff::')
			subnet = IP(ip+'/48', make_net=True)
			v6net.add(subnet)

	print "24-bit net prefix ipv4:", len(v4net), "IPv4 addresses:",ipv4
	print "48-bit net prefix ipv6:", len(v6net), "IPv6 addresses:",ipv6
	print "number of distinctive client IPs:", len(clientIPs)

	of.write( "IPv4 addresses:"+ str(ipv4) +"24-bit net prefix ipv4:"+ str(len(v4net)) ) 
	of.write( "IPv6 addresses:"+ str(ipv6) +"48-bit net prefix ipv6:"+ str(len(v6net)) ) 
	of.write( "number of distinctive client IPs:"+ str(len(clientIPs)) )
